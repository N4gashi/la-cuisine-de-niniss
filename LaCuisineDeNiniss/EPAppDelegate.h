//
//  EPAppDelegate.h
//  LaCuisineDeNiniss
//
//  Created by gep_rcdsm on 14/01/2014.
//  Copyright (c) 2014 GEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
