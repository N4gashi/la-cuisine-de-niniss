//
//  main.m
//  LaCuisineDeNiniss
//
//  Created by gep_rcdsm on 14/01/2014.
//  Copyright (c) 2014 GEP. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EPAppDelegate class]));
    }
}
